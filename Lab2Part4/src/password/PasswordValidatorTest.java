package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import time.Time;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = NullPointerException.class)
	public void testCheckPasswordLength() {
		boolean result = PasswordValidator.checkPasswordLength((String)null);
		assertTrue("Passed a null value", result == false);
	}
	
	@Test
	public void testCheckPasswordLengthOk() {
		boolean result = PasswordValidator.checkPasswordLength("abcd1234");
		assertTrue("Length is ok", result == true);
	}
	
	@Test
	public void testCheckPasswordLengthBoundryOut() {
		boolean result = PasswordValidator.checkPasswordLength("abcd123");
		assertTrue("Length not ok", result == false);
	}
	
	@Test
	public void testCheckPasswordLengthBoundryIn() {
		boolean result = PasswordValidator.checkPasswordLength("abcd12345");
		assertTrue("Length is ok", result == true);
	}

	@Test
	public void testCheckDigits() {
		boolean result = PasswordValidator.checkDigits("abcdefgh");
		assertTrue("No digits", result == false);
	}
	
	@Test
	public void testCheckDigitsOk() {
		boolean result = PasswordValidator.checkDigits("abcd1234");
		assertTrue("Password ok", result == true);
	}
	
	@Test
	public void testCheckDigitsBoundryOut() {
		boolean result = PasswordValidator.checkDigits("abcde123");
		assertTrue("Length not ok", result == true);
	}
	
	@Test
	public void testCheckDigitsBoundryIn() {
		boolean result = PasswordValidator.checkDigits("abcdefg5");
		assertTrue("Length is ok", result == false);
	}
	
	

}
