package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalSeconds() {
		int seconds = Time.getTotalSeconds("12:05:05");
		assertTrue("The seconds were not calc properly", seconds==43505);
	}

	@Test
	public void testGetSeconds() {
		int seconds = Time.getSeconds("12:05:05");
		assertTrue("The seconds were not calc properly", seconds==05);
	
	}
	
	@Test
	public void testGetMiliSeconds() {
		int milliSeconds = Time.getMilliSeconds("12:05:05:05");
		assertTrue("The seconds were not calc properly", milliSeconds==05);
	
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetMiliSecondsBoundaryOut() {
		int result = Time.getMilliSeconds("12:05:05:1000");
		assertTrue("The seconds were not calc properly", result == 0);
	
	}
	
	


}
